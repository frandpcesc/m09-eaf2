$(document).ready(function(){
    var audio = $('#audio').get(0);
    $('#reproduir').on('click', playPause);
    $('#pujar').on('click', pujarVolum);
    $('#baixar').on('click', baixarVolum);
    $('#muted').on('click', silenciar);
    
    function playPause() {
        if(audio.paused || audio.ended){
            audio.play();
        }else {
            audio.pause();
        }
    }
    
    function pujarVolum() {
        if(audio.volume >= 0.0 && audio.volume < 1){
            audio.volume += 0.25;
        }
    }
    
    function baixarVolum() {
        if(audio.volume > 0.0 && audio.volume <= 1.0) {
            audio.volume -= 0.25;
        }
    }
    
    function silenciar() {
        if(audio.muted) {
            audio.volume = 0.5;
            audio.muted = false;
        }else {
            audio.volum = 0.0
            audio.muted = true;
        }
    }

});