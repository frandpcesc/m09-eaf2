$(document).ready(function () {

    /* Codi Semàfor */
    var taronja = setInterval(semaforTaronja, 5000);
    var vermell = setInterval(semaforVermell, 7000);
    var verd = setInterval(semaforVerd, 12000);

    function semaforTaronja() {
        $('#vermell').css('opacity', 0.2);
        $('#verd').css('opacity', 0.2);
        $('#taronja').css('opacity', 1);
        clearInterval(taronja);
    }

    function semaforVermell() {
        $('#verd').css('opacity', 0.2);
        $('#taronja').css('opacity', 0.2);
        $('#vermell').css('opacity', 1);
        clearInterval(vermell);        
    }

    function semaforVerd() {
        $('#vermell').css('opacity', 0.2);
        $('#taronja').css('opacity', 0.2);
        $('#verd').css('opacity', 1);
        clearInterval(verd);
        taronja = setInterval(semaforTaronja, 5000);
        vermell = setInterval(semaforVermell, 7000);
        verd = setInterval(semaforVerd, 12000);
    }

    /* Codi Canvas */
    var canvas, context, canvaso, contexto;
    canvaso = document.getElementById('imageView');
    context = canvaso.getContext('2d');
    context.lineWidth = 5;
    
    context.strokeStyle = '#000000';
    context.save();
    context.translate(56.5, 143);
    context.scale(1, 0.5217391304347826);
    context.beginPath();
    context.arc(0, 0, 26, 0, 6.283185307179586, false);
    context.stroke();
    context.closePath();
    context.restore();
    
    context.strokeStyle = '#000000';
    context.beginPath();
    context.moveTo(85, 139);
    context.lineTo(85, 47);
    context.stroke();
    context.closePath();
    
    context.strokeStyle = '#000000';
    context.save();
    context.translate(158.5, 137.5);
    context.scale(1, 0.4782608695652174);
    context.beginPath();
    context.arc(0, 0, 25, 0, 6.283185307179586, false);
    context.stroke();
    context.closePath();
    context.restore();
    
    context.strokeStyle = '#000000';
    context.beginPath();
    context.moveTo(87, 51);
    context.lineTo(191, 51);
    context.stroke();
    context.closePath();
    
    context.strokeStyle = '#000000';
    context.beginPath();
    context.moveTo(188, 48);
    context.lineTo(188, 135);
    context.stroke();
    context.closePath();
});