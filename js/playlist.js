$(document).ready(function(){
  var reproductor = videojs('player');
  
  reproductor.playlist([{
    sources: [{
      src: 'videos/heathens.mp4',
      type: 'video/mp4'
    }],
  }, {
    sources: [{
      src: 'videos/stressedOut.mp4',
      type: 'video/mp4'
    }],
  }, {
    sources: [{
      src: 'videos/ride.mp4',
      type: 'video/mp4'
    }],
  }]);
  
  reproductor.playlist.autoadvance(0);

  $("#seguent").click(function(){
    reproductor.playlist.next();
  });

  $("#anterior").click(function(){
    reproductor.playlist.previous();
  });

});

